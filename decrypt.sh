#!/bin/bash

IFS='
'

dir=$1

if [ -z $dir ]
then
	echo "Usage: $0 <directory>"
	exit 1
fi

if [ ! -d $dir ]
then
	echo "Directory not found: $dir"
	exit 2
fi

if [ ! -x $dir ]
then
	echo "Directory not accessible: $dir"
	exit 3
fi

stty -echo
echo -n "Enter Password: "
read pass
stty echo
echo

for file in `find $1 -type f | grep -E "\.crypted$"`
do
	dest=`echo $file | sed -e 's/\.crypted$//'`
	if [ "$dest" != "$file" ]
	then
		echo -n "- $file => $dest : "
		if cat "$file" | mdecrypt -q -k $pass > "$dest"
		then
			echo "OK"
			if ! rm -f "$file"
			then
				echo "Error removing $file"
			fi
		else
			echo "ERROR"
			rm -f "$dest"
		fi
	fi
done

exit 0
