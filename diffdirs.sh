#!/bin/bash

if [ $# -ne 2 ]
then
	echo "Uso: $0 <dir1> <dir2>"
	exit 1
fi

dir1="$1"
dir2="$2"
list="/tmp/$$.list"
cont="/tmp/$$.cont"

cd $dir1
find -type f | sed 's/^\.\///' > $list
cd - > /dev/null

echo "Analisando diferenças entre diretórios:"
echo "- $dir1"
echo "- $dir2"

while read arq
do
	if ! diff $dir1/$arq $dir2/$arq > $cont
	then
		echo -e "\n$arq:"
		cat $cont
	fi
done < $list

rm -f $list

exit 0
