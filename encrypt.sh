#!/bin/bash

IFS='
'

dir=$1

if [ -z $dir ]
then
	echo "Usage: $0 <directory>"
	exit 1
fi

if [ ! -d $dir ]
then
	echo "Directory not found: $dir"
	exit 2
fi

if [ ! -x $dir ]
then
	echo "Directory not accessible: $dir"
	exit 3
fi

stty -echo
echo -n "Enter Password: "
read pass
stty echo
echo

stty -echo
echo -n "Reenter Password: "
read pass2
stty echo
echo

if [[ $pass != $pass2 ]]
then
	echo "Passwords do not match"
	exit 4
fi

echo "Encrypting directory $dir..."

for file in `find $1 -type f | grep -v -E "\.crypted$"`
do
	dest="${file}.crypted"
	if [ "$dest" != "$file" ]
	then
		echo -n "- $file => $dest : "
		if cat "$file" | mcrypt -a blowfish -q -k $pass > "$dest"
		then
			echo "OK"
			if ! rm -f "$file"
			then
				echo "Error removing $file"
			fi
		else
			echo "ERROR"
		fi
	else
		echo "$file is already encrypted"
	fi
done

exit 0
