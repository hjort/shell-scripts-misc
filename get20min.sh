#!/bin/bash

# This Shell Script is intented to automatically download
# and open the "20 Minutes" french daily newspaper.

DATE=`date +%Y%m%d`
CITY="PAR" # Paris
URL="http://pdf.20minutes.fr/${DATE}_${CITY}.pdf"
#URL="http://www.20minutes.fr/media/journal/${DATE}_${CITY}/sources/projet/pdfweb.pdf"
DEST="$HOME/20min"
FILE="$DEST/$DATE.pdf"

if [ ! -d $DEST ]
then
	echo "Creating destination directory..."
	mkdir -p $DEST
fi

if [ ! -f $FILE ]
then
	echo "Retrieving newspaper file... $URL"
	wget $URL -O $FILE

	if [ $? -ne 0 ]
	then
		echo "An error occurred while downloading the file"
	fi
else
	echo "File was already downloaded for today"
fi

if [ -f $FILE ]
then
	echo "Opening retrieved PDF file..."
	evince $FILE
fi

exit 0

